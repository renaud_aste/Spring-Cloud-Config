package com.aste.server;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Value("${name}")
    private String name;

    @RequestMapping("/")
    public String index() {
        return String.format("Greetings from Spring Boot! [%s]", name);
    }

}